# sofcp v1.0 Release

A simple `cp` clone made for my own personal use on my projects.\
Works with [Glob Pattern Matching](#glob-patterns) on files.


Why:\
Was annoying how I couldn't do eg `cp app/*.hs src/` with `cp`.\
With this tool, you can do `sofcp app/ '*.hs' src/`.

## Usage

### _**`sofcp <dir> <pattern> <dest> [_ignored_]...`**_
**Copies** all files in directory `<dir>`,\
Matching glob pattern `<pattern>`, (see [Glob Patterns](#glob-patterns))\
Into directory `<dest>` with same names (/ relative paths). \(**Note**: This will **overwrite** any files if need be.)\
Extra args (ie `[_ignored_]...`) will have no effect.

#### Example
`sofcp directory/ source.file destination/`\
Copies `directory/source.file` into `destination/source.file`. (overwrites if it already exists.)


`sofcp . '*.o' dest/`\
Copies all files that end in `.o` into the `dest/`. (overwriting any that already exist.)

---

### _**`sofcp -f <dir> <pattern> [_ignored_]...`**_
**Displays** the names of the files that **would** be copied.
In directory `<dir>`,\
Matching glob pattern `<pattern>`. (see [Glob Patterns](#glob-patterns))\
Extra args (ie `[_ignored_]...`) will have no effect.

#### Example
`sofcp -f . '*.o'`\
Shows all files that end in `.o` in the current directory. (+ Numbering)

---

### _**`sofcp -h [_ignored_]...`**_
**Displays** help message & all args.

#### Example
`sofcp -h foo bar`\
Displays help message with the following appended:
```
Args were:
1: -h
2: foo
3: bar
```


## Glob Patterns
`*` matches 0 or more characters.\
`?` matches 0 or 1 characters.\
`[...]` acts as [regex bracket expression](https://pubs.opengroup.org/onlinepubs/009604499/basedefs/xbd_chap09.html#tag_09_03_05) but `!` acting as negation instead of `^`.


**Further refrence**: [The Open Group Base Specifications Issue 6 : Glob Patterns](https://pubs.opengroup.org/onlinepubs/009604499/utilities/xcu_chap02.html#tag_02_13)


## Licence

```
Copyright (C) 2023 sof

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
