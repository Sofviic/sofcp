module Main (main) where

import qualified System.Directory as D
import System.Environment
import System.FilePath
import System.FilePath.Glob

main :: IO ()
main = execCommand

execCommand :: IO ()
execCommand = do
                args <- getArgs
                if length args < 3 || args !! 0 == "-h"
                    then usage
                    else if args !! 0 == "-f"
                            then do let (_:dir:src:_) = args
                                    findFiles dir src
                            else do let (dir:src:dst:_) = args
                                    copyFiles dir src dst


usage :: IO ()
usage = do 
            putStrLn "Usage: sofcp <dir> <pattern> <dest> | sofcp -f <dir> <pattern> | sofcp -h"
            putStrLn "- Copies all files matching <pattern> pattern to <dest> directory."
            putStrLn "- Relative path to <dir>."
            putStrLn "- Note: uses glob patterns. see https://pubs.opengroup.org/onlinepubs/009604499/utilities/xcu_chap02.html#tag_02_13"
            putStrLn "- If first arg is '-f', return the names of the files that **would** be copied."
            putStrLn "- If first arg is '-h', shows this message with list of args below."
            putStrLn "Example: sofcp directory/ source.file destination/"
            putStrLn "- Copies directory/source.file into destination/source.file"
            putStrLn "Example: sofcp . '*.o' dest/"
            putStrLn "- Copies all files that end in '.o' into the dest/"
            putStrLn "Example: sofcp -f . '*.o'"
            putStrLn "- Shows all files that end in '.o' in the current directory"
            putStrLn ""
            putStrLn "Args were:"
            displayArgs
            putStrLn ""

copyFiles :: FilePath -> String -> FilePath -> IO ()
copyFiles fp pattern dest = do 
                                absfiles <- globDir1 (compile pattern) fp
                                let files = fmap (makeRelative fp) absfiles
                                fmap (const ()) 
                                    . sequence 
                                    . fmap (\f -> (fp </> f) `D.copyFile` (dest </> f)) 
                                    $ files

findFiles :: FilePath -> String -> IO ()
findFiles fp pattern = do
                        files <- globDir1 (compile pattern) fp
                        putStrLn "Files found:"
                        displayNumbered 0 files

displayArgs :: IO ()
displayArgs = getArgs >>= displayNumbered 1

displayNumbered :: Int -> [String] -> IO ()
displayNumbered n = fmap (const ())
                    . sequence
                    . fmap (putStrLn . \(i,e) -> show i ++ ": " ++ e)
                    . zip [n..]


